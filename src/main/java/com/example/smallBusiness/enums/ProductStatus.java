package com.example.smallBusiness.enums;

public enum ProductStatus {
    SOLD,
    BOOKED,
    AVAILABLE
}
