package com.example.smallBusiness.model;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
@Data
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    public String status;

    public LocalDate dateOfBook;
    @ManyToOne
    public Product product;
    @ManyToOne
    @JoinColumn(name = "user_id")
    public User user;

}
