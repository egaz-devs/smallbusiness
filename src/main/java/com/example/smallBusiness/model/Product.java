package com.example.smallBusiness.model;


import com.example.smallBusiness.enums.ProductStatus;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    @Column(name = "product_name",nullable = false)
    public String productName;
    public String productCategory;
    public String price;
    @Enumerated(EnumType.STRING)
    public ProductStatus status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User user;
}
