package com.example.smallBusiness.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    public Long id;
    public String firstName;
    public String lastName;
    public String address;
    public String password;
    public String phone;
    public String status;
}
