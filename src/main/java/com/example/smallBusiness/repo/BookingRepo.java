package com.example.smallBusiness.repo;

import com.example.smallBusiness.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepo extends JpaRepository<Booking,Long> {
}
