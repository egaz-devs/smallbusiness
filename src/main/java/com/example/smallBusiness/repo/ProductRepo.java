package com.example.smallBusiness.repo;

import com.example.smallBusiness.enums.ProductStatus;
import com.example.smallBusiness.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepo extends JpaRepository<Product,Long> {

    List<Product>  findByProductCategory(String p);
    List<Product> findByProductName(String p);
    List<Product>  findByProductNameAndPrice(String pName,String price);


    @Query(value ="select * from product where product.status = ?1",nativeQuery = true)
    List<Product>  findByMyOther(ProductStatus s);

    @Query("SELECT p from Product p where status = ?1")
    List<Product>  findByMyOtherJpQl(ProductStatus s);




}
