package com.example.smallBusiness.repo;

import com.example.smallBusiness.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepo extends JpaRepository<User,Long> {
}
