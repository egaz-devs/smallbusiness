package com.example.smallBusiness.service;

import com.example.smallBusiness.model.User;
import com.example.smallBusiness.repo.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {
    @Autowired
    private UsersRepo usersRepo;


    
    public User addUser(User request){
     
        return usersRepo.save(request);
    }
    public List<User> getAllUser(){
        return usersRepo.findAll();
    }
    public Optional<User> findById(Long id){
        return usersRepo.findById(id);
    }

    public User updateUser(User request,Long id){
        request.setId(id);
        return usersRepo.save(request);
    }
}
