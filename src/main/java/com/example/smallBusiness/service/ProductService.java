package com.example.smallBusiness.service;

import com.example.smallBusiness.model.Product;
import com.example.smallBusiness.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepo productRepo;


    public Product addProduct(Product request){

        return productRepo.save(request);
    }
    public List<Product> getAllProduct(){
        return productRepo.findAll();
    }
    public Optional<Product> findById(Long id){
        return productRepo.findById(id);
    }

    public Product updateProduct(Product request,Long id){
        request.setId(id);
        return productRepo.save(request);
    }
}
