package com.example.smallBusiness.service;

import com.example.smallBusiness.model.Booking;
import com.example.smallBusiness.repo.BookingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookingService {
    @Autowired
    private BookingRepo bookingRepo;


    public Booking addBooking(Booking request){

        return bookingRepo.save(request);
    }
    public List<Booking> getAllBooking(){
        return bookingRepo.findAll();
    }
    public Optional<Booking> findById(Long id){
        return bookingRepo.findById(id);
    }

    public Booking updateBooking(Booking request,Long id){
        request.setId(id);
        return bookingRepo.save(request);
    }
    
}
