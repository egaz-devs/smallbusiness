package com.example.smallBusiness.controller;

import com.example.smallBusiness.model.User;
import com.example.smallBusiness.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/auth")
public class UserController {
    @Autowired
    private UsersService userService;

    @GetMapping("/")
    public ResponseEntity<?> getAllUser(){
        List<User> list = userService.getAllUser();
      
        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getByUser(@PathVariable Long id){
        Optional<User> s = userService.findById(id);
        return ResponseEntity.ok(s);
    }

    @PostMapping("/add")
    public User addUser(@RequestBody User n){
        return userService.addUser(n);
    }
    @PostMapping("/update")
    public User updateUser(@RequestBody User n,@RequestParam Long id){
        return userService.updateUser(n,id);
    }
}
