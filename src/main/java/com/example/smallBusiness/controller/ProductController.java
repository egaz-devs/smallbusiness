package com.example.smallBusiness.controller;

import com.example.smallBusiness.model.Product;
import com.example.smallBusiness.model.Product;
import com.example.smallBusiness.repo.ProductRepo;
import com.example.smallBusiness.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductRepo productRepo;

    @GetMapping("/")
    public ResponseEntity<?> getAllProduct(){
        List<Product> list = productService.getAllProduct();

        return ResponseEntity.ok(list);
    }
    @GetMapping("/product-name/{pname}")
    public ResponseEntity<?> getByProductName(@PathVariable String pname){
        List<Product> list = productRepo.findByProductName(pname);

        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getByProduct(@PathVariable Long id){
        Optional<Product> s = productService.findById(id);
        return ResponseEntity.ok(s);
    }

    @PostMapping("/add")
    public Product addProduct(@RequestBody Product n){
        return productService.addProduct(n);
    }
    @PostMapping("/update")
    public Product updateProduct(@RequestBody Product n,@RequestParam Long id){
        return productService.updateProduct(n,id);
    }
}
